#!/bin/bash

clear; echo "Copy started..."

# mv ~/.opam ~/afs/; clear; echo "Copie terminee !"
rsync -Pr ~/.opam ~/afs/; clear; echo "Copy done !"

echo "Making the symbolic link..."
echo exec --no-startup-id ln -s ~/afs/.opam ~/.opam >> ~/.config/i3/config

ln -s ~/afs/.opam ~/.opam

rm ./opam_into_afs.sh